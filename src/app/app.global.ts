export const App: any = {
  id: 'AllIsShop',
  ContactEmail    : 'allisshop@gmail.com', // this email wiil be used for email in Account Menu -> Contact (sending email)
  url: 'https://www.allisshop.com',
  OneSignalAppID: '',
  GCM: '', // Google Project Number for Google Cloud Messaging
  consumerKey: 'ck_ccc13af0f5231cf91f6c878f3a667648f555a786',
  consumerSecret: 'cs_6c8b28efbf8b9044b0ffbefcbdd36c0bdb9409da',

  // PAYPAL
  //paypalSandboxClientID: 'AZjyISbp1zmOhZ0o_iAG3W2IGjlz2hvEC-8cGoQ7fXcMFN9afaRuW0X1B1PVSgkSuTQWOKqM9N4NTkOP',
  //paypalLiveClientID: '', // get this from paypal developer dashboard
  //paypalState: 'PayPalEnvironmentSandbox', // change this to 'PayPalEnvironmentProduction' if you wanna live

  // RazorPay
  RazorColor: '#FF1654', // this will be your coloring style in RazorPay when checkout
  logo: '', // change this with your own logo
  title: 'AllIsShop', // this will be your app name shown in RazorPay when checkout

  languages: [
    {id: 'en', title: 'English'},
    {id: 'id', title: 'Indonesian'},
    {id: 'fr', title: 'French'},
    {id: 'in', title: 'Hindi'},
    {id: 'cn', title: 'Chinese'}
  ],

  defaultLang: 'en'
};