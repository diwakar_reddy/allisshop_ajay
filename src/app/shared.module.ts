import { COMPONENTS, DIRECTIVES, PIPES } from './app.imports';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    PIPES,
    COMPONENTS,
    DIRECTIVES
  ],
  imports: [
    IonicModule,
    IonicImageViewerModule
  ],
  exports: [
    PIPES,
    COMPONENTS,
    DIRECTIVES,
    TranslateModule
  ]
})

export class SharedModule { }