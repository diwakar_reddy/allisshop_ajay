// Ionic native providers
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { PayPal } from '@ionic-native/paypal';
import { Stripe } from '@ionic-native/stripe';
import { EmailComposer } from '@ionic-native/email-composer';
import { OneSignal } from '@ionic-native/onesignal';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AppRate } from '@ionic-native/app-rate';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';

// Modules
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicImageViewerModule } from 'ionic-img-viewer';

// Directives
import { AnimateItemSliding } from '../directives/directives';

// Components
import { IonRating, FaIconComponent } from '../components/components';

// Pipes
import { MomentPipe, DiscountPipe, MoneyPipe } from '../pipes/pipes';

// Providers
import { UserProvider, BlogProvider, ToastProvider, SettingsProvider, LoadingProvider, NotifProvider, AddressProvider, OrderProvider, WooCommerceProvider, CartProvider, WishlistProvider, HistoryProvider } from '../providers/providers';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/languages/', '.json');
}

import { App } from './app.global';

export const MODULES = [
  HttpModule,
  BrowserModule,
  IonicImageViewerModule,
  // database name for cart, wish, checkout, etc
  IonicStorageModule.forRoot({
    name: '_'+App.id,
       driverOrder: ['indexeddb', 'sqlite', 'websql']
  }),
  TranslateModule.forRoot({
    loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
    }
  })
];

export const PIPES = [
  MomentPipe,
  DiscountPipe,
  MoneyPipe
];

export const PROVIDERS = [
  //AppState,
  UserProvider,
  OrderProvider,
  BlogProvider,
  AddressProvider,
  WooCommerceProvider,
  SettingsProvider,
  WishlistProvider,
  CartProvider,
  HistoryProvider,
  LoadingProvider,
  NotifProvider,
  ToastProvider,

    // Ionic native specific providers
  IonicStorageModule,
  StatusBar,
  SplashScreen,
  PayPal,
  Stripe,
  OneSignal,
  SocialSharing,
  AppRate,
  Keyboard,
  AppVersion,
  EmailComposer
];

export const COMPONENTS = [
  IonRating,
  FaIconComponent
];

export const DIRECTIVES = [
  AnimateItemSliding
];