
import { Component } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import { TranslateService } from '@ngx-translate/core';
import { Platform /*,Config*/ } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { WooCommerceProvider, NotifProvider, SettingsProvider } from '../providers/providers';
import { App as ion } from './app.global';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage: any = 'TabsPage';
  app: any = {};

  constructor(private oneSignal: OneSignal, private notif: NotifProvider, private platform: Platform, /*private config: Config,*/ public settings: SettingsProvider, private translate: TranslateService, private woo: WooCommerceProvider, private statusBar: StatusBar) {
    this.settings.load().then((x) => {
      this.woo.loadZones();
      this.app = x;
      this.initTranslate();
    });

    this.platform.ready().then(() => { 
      if (this.platform.is('cordova')) {
        this.statusBar.show();
        //if (cordova.platformId == 'android') {
          statusBar.backgroundColorByHexString("#191d3d");
      //}
        this.oneSignal.startInit(ion.OneSignalAppID, ion.GCM);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        
        this.oneSignal.handleNotificationReceived().subscribe((x) => {
        // do something when notification is received
          console.log(x);
          this.notif.post(x.payload);
        });
        
        this.oneSignal.handleNotificationOpened().subscribe((x) => {
          // do something when a notification is opened
          //this.notif.post(x.payload);
        });
        
        this.oneSignal.endInit();
      }
    });
  }

  ionViewCanEnter(){
    
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang(this.app && this.app.language !== undefined ? this.app.language : ion.defaultLang);

    if(this.app && this.app.language !== undefined)
      this.translate.use(this.app.language);
    else
      this.translate.use(ion.defaultLang);
      
    // this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
    //   this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    // });
  }

}
