import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, LoadingController, Refresher, Platform, Events, ModalController, NavController } from 'ionic-angular';
import { UserProvider, WooCommerceProvider } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  status: string = "unpaid";
  orders: any = {
    paid: [],
    processing: [],
    unpaid: []
  };

  load: any; 

  constructor(private loader: LoadingController, private statusBar: StatusBar, private platform: Platform, public nav: NavController, private events: Events, private modal: ModalController, private user: UserProvider, private woo: WooCommerceProvider) {
    this.listenIsLoggedIn();
    this.listenIsLoggedOut();
  }

  ionViewWillEnter(){
    if (this.platform.is('cordova')) 
      this.statusBar.styleLightContent();

    this.setForUser();
  }

  listenIsLoggedIn(){
    this.events.subscribe('user:login', (res) => {
      this.setForUser();
    });
  }

  listenIsLoggedOut(){
    this.events.subscribe('user:logout', () => {
      this.setRootForGuest();
    });
  }

  setRootForGuest(){
    this.orders = {
      paid: [],
      processing: [],
      unpaid: []
    }
  }

  setForUser(status: string = ''){
    if(this.user.all){
      this.load = this.loader.create({
        spinner: 'dots'
      });
  
      this.load.present();
      this.woo.getOrders(this.user.id, null, null, status).then( x=> {
        this.load.dismiss();
        this.orders = x;        
      }, err => {
        this.load.dismiss();    
      });
    }else 
      this.setRootForGuest();
  }

  doRefresh(status: string, refresher: Refresher) {
    this.setForUser(status);
    refresher.complete();
  }

  doPulling(refresher: Refresher) {
    // console.log('DOPULLING', refresher.progress);
  }

  login(){
    this.modal.create('LoginPage', {}).present();
  }

  goHome(){
    this.nav.parent.select(0);
  }

  goTo(params){
    this.nav.push('OrderDetailPage', {params: params});
  }

}
