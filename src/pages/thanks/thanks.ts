import { Component } from '@angular/core';
import { IonicPage, Platform, AlertController, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-thanks',
  templateUrl: 'thanks.html',
})
export class ThanksPage {
  rootPage: any;
  data: any;

  constructor(private nav: NavController, private translate: TranslateService, public alert: AlertController, public navParams: NavParams, private platform: Platform) {
    this.data = this.navParams.data.params;

    // this.platform.ready().then(() => { 
    //   this.platform.registerBackButtonAction(() => {
    //       this.confirmHome();
    //   });
    // });
  }

  confirmHome() {
    this.translate.get(['BACKHOME', 'BACKHOME_MSG', 'CANCEL', 'YES']).subscribe( x=> {
      this.alert.create({
        title: x.BACKHOME,
        message: x.BACKHOME_MSG,
        buttons: [{
            text: x.CANCEL
          },{
            text: x.YES,
            handler: () => {
              this.goHome();
            }
          }]
      }).present();
    });
  }

  exitApp(){
    this.platform.exitApp();
  }

  goHome(){
    this.nav.setRoot('TabsPage');
  }

}
