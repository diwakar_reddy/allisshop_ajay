import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, Platform, NavController } from 'ionic-angular';
import { WooCommerceProvider, LoadingProvider } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  categories: any;

  constructor(private statusBar: StatusBar, private platform: Platform, private nav: NavController, private loader: LoadingProvider, private woo: WooCommerceProvider) {
    this.loader.present();
    this.woo.getCategories(0).then( x=> {
      this.categories = x;
      for(let i in x){
        let child = [];
        child.push({id: x[i].id, count: x[i].count, image: x[i].image, name: 'All '+x[i].name, slug: x[i].slug});
        this.woo.getCategories(x[i].id).then( y=> {
          child = child.concat(y);
          this.categories[i].child = child;
        });
      }
      this.loader.dismiss();
    })
  }

  ionViewDidLoad(){
    
  }

  ionViewWillEnter(){
    if (this.platform.is('cordova')) 
      this.statusBar.styleLightContent();
  }

  toggleSection(i) {
    this.categories[i].open = !this.categories[i].open;
  }

  toggleItem(i, j) {
    this.categories[i].child[j].open = !this.categories[i].child[j].open;
  }

  goTo(page, params){
    this.nav.push(page, {params: params});
  }

}
