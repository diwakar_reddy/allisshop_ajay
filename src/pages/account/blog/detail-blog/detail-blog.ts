import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detail-blog',
  templateUrl: 'detail-blog.html',
})
export class DetailBlogPage {
  blog: any;

  constructor(public param: NavParams) {
    this.blog = param.data.params;
    console.log(this.blog);
  }
}
