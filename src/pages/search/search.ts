import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { IonicPage, Platform, App, ViewController, NavController, AlertController } from 'ionic-angular';
import { WooCommerceProvider, ToastProvider, LoadingProvider, HistoryProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})

export class SearchPage {
	private search : FormGroup;

	showCancel: boolean;
	products: any;

	categories: any = [];
	app: any;

	constructor(private statusBar: StatusBar, private platform: Platform, private translate: TranslateService, private keyboard: Keyboard, fb: FormBuilder, public history: HistoryProvider, private toast: ToastProvider, private loader: LoadingProvider, public appCtrl: App, public alertCtrl: AlertController, public viewCtrl: ViewController, public navCtrl: NavController, private woo: WooCommerceProvider) {
	
		this.loader.present();
		
		this.search = fb.group({
			search: ''
		});

		this.woo.getCategories(0).then( x=> {
		  this.categories = x;
		  for(let i in x){
			let child = [];
			child.push({id: x[i].id, count: x[i].count, image: x[i].image, name: 'All '+x[i].name, slug: x[i].slug});
			this.woo.getCategories(x[i].id).then( y=> {
			  child = child.concat(y);
			  this.categories[i].child = child;
			});
		  }
		  this.loader.dismiss();

		})

	}

	onCancel(e){
		console.log(e);
	}

	submit(){
		this.goTo('ProductGridPage', this.search.value);
	}

	toggleSection(i) {
		this.categories[i].open = !this.categories[i].open;
	}

	toggleItem(i, j) {
		this.categories[i].children[j].open = !this.categories[i].children[j].open;
	}

	ionViewWillEnter(){
		if(this.platform.is('cordova'))
			this.statusBar.styleLightContent();
		this.loadHistory();
	}
	
	loadHistory(){
		if(this.platform.is('cordova'))
			this.statusBar.styleLightContent();
		this.history.load().then(() => { });
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}

	reset(e){
		this.search.reset();
	}

	confirmRemove(){
		this.history.clear;
		this.translate.get(['HISTORY_CLEAR']).subscribe( x=> {
			this.toast.show(x.HISTORY_CLEAR);
		});
	}

	resetHistory(product) {
		this.translate.get(['HISTORY_TITLE', 'HISTORY_DESC', 'CANCEL', 'YES']).subscribe( x=> {
			this.alertCtrl.create({
				title: x.HISTORY_TITLE,
				message: x.HISTORY_DESC,
				buttons: [{
					text: x.CANCEL
				},{
					text: x.YES,
					handler: () => {
						this.confirmRemove();
					}
				}]
			}).present();
		});
	}

	goTo(page: string, params: any){
		this.dismiss();
		this.appCtrl.getRootNav().push(page, {params: params});
	}

}
