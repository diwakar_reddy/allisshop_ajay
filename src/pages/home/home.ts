import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { IonicPage, Platform, NavController, ModalController } from 'ionic-angular';
import { WooCommerceProvider, ToastProvider, LoadingProvider, WishlistProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { App } from '../../app/app.global';

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {
	App: any;
	categories: any[] = [];
	data: any[] = [];
	banners: any[] = [];
	app: any;
	reloadSlider: boolean;

	constructor(public nav: NavController, private platform: Platform, private keyboard: Keyboard, private statusBar: StatusBar, private translate: TranslateService, private toast: ToastProvider, public wishlist: WishlistProvider, public loader: LoadingProvider, public modalCtrl: ModalController, private woo: WooCommerceProvider) {
		try {
			window["cordova"].plugins.certificates.trustUnsecureCerts(true);
		} catch (ex) {
			console.log(" certificate set ", ex);
		}
		this.loader.present();

		this.woo.getAllCategories(10).then((tmp) => { // get all categories
			this.categories = tmp;
			let root = [];
			//console.log("tmp :", tmp);
			this.woo.loadSetting().then(x => {
				this.app = x;
				console.log(x);
				if (x.currency) {
					for (let i in tmp) {
						if (tmp[i].count >= 0) {
							let item = {
								data: tmp[i],
								items: []
							};
							this.woo.getAllProducts(null, tmp[i].id, null, null, null, 9, null, 'asc', 'title', null, null).then((val) => {
								item.items = val;
								root.push(item);
							})
						}
					}
				}
			});
			//console.log("root : ",root);
			this.data = root;
			this.loader.dismiss();
		}, err => {
			this.loader.dismiss();
			this.toast.show(err);
		})

		// this.woo.loadBanners().then(
		// 	(data) => {
		// 		console.log(" data ", data);
		// 		this.banners = data;
		// 	},
		// 	(error) => {
		// 		console.log(" error ", error);
		// 	}
		// );
	}

	ionViewWillEnter() {
		if (this.platform.is('cordova')) {
			this.statusBar.styleLightContent();
			this.keyboard.disableScroll(false);
			this.keyboard.hideKeyboardAccessoryBar(false);
		}
	}

	setFav(product: any) {
		this.translate.get(['REMOVE_WISH', 'ADDED_WISH']).subscribe(x => {
			let msg = product.isFav ? x.REMOVE_WISH : x.ADDED_WISH;
			this.wishlist.post(product);
			product.isFav = product.isFav ? false : true;
			this.toast.show(msg);
		});
	}

	ionViewDidEnter() {
		try {
			let event = document.createEvent('HTMLEvents');
			event.initEvent('resize', true, false);
			window.dispatchEvent(event);
		} catch (e) { }
	}

	showSearch() {
		this.modalCtrl.create('SearchPage').present();
	}

	goTo(page, params) {
		this.nav.push(page, { params: params });
	}

}
