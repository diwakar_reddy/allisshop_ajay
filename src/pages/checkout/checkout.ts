import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, App, Platform, AlertController, NavController, Events, ModalController, NavParams } from 'ionic-angular';
import { AddressProvider, SettingsProvider, ToastProvider, UserProvider, LoadingProvider, CartProvider, WooCommerceProvider, OrderProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})

export class CheckoutPage {
  checkout: string = "shipping";  
  cart: any;
  settings: any;
  billing: any;
  shipping: any;
  zones: any;
  shipping_lines: any[] = [];
  shipping_method: any = [];
  payments: any[] = [];
  coupons: any[] = [];
  tax: any;
  order: any = {};
  total: number = 0;
  allowFreeShipping: boolean = false;

  stripe: any = {
    no: '',
    month: '',
    year: ''
  };

  razor: any = {};

  coupon: any = {};

  months: any = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  years: any = [];

  img: any = {
    stripe: [
      'assets/img/logo/visa.svg',
      'assets/img/logo/mastercard.svg',
      'assets/img/logo/amex.svg',
      'assets/img/logo/diners.svg',
      'assets/img/logo/discover.svg',
      'assets/img/logo/jcb.svg'
    ],
    paypal: [
      'assets/img/logo/visa.svg',
      'assets/img/logo/mastercard.svg',
      'assets/img/logo/amex.svg',
      'assets/img/logo/discover.svg'
    ],
    razorpay: [
      'assets/img/logo/logo-razor.png'
    ]
  };

  constructor(private setting: SettingsProvider, private app: App, private statusBar: StatusBar, private alert: AlertController, private platform: Platform, private nav: NavController, private translate: TranslateService, private toast: ToastProvider, private user: UserProvider, private loader: LoadingProvider, private woo: WooCommerceProvider, private _cart: CartProvider, private events: Events, private _order: OrderProvider, private address: AddressProvider, public navParams: NavParams, public modal: ModalController) {
    this.woo.loadPayments().then( x=> {
      this.payments = x;
    });

    this.woo.loadCoupons().then( x=> {
      this.coupons = x;
    }, err => {
      console.log(err);
    })

    this.cart = this._cart;

    if(this.setting.all.zones.length <= 0){
      this.loader.present();
      this.woo.loadZones();
      this.setting.load().then(x=>{
        this.zones = x.zones;
        this.loader.dismiss();
      })
    }else
      this.zones = this.setting.all.zones;

    this.listenOrder();
  }

  ionViewDidEnter(){
    if (this.platform.is('cordova')) 
      this.statusBar.styleLightContent();
    this.setOrder();
  }

  setOrder(){

    if(this.address.getPrimary){
      this.billing = this.address.getPrimary;
      this.shipping = this.address.getPrimary;
    }

    if(this._order.billing)
      this.billing = this._order.billing;
  
    if(this._order.shipping)
      this.shipping = this._order.shipping;

    //let tmp = (this.settings.value == 'shipping') ? this.shipping : this.billing;

    if(this.shipping){
      let id = this.woo.getSingleZone(this.zones, this.shipping);
      if(id){
        this.loader.present();
        this.woo.getShippingZoneMethod(id).then( x=> {
          this.shipping_method = x;
          this.loader.dismiss();
          let tmp;
          for(let i in x){
            if(x[i].enabled && x[i].method_id == 'free_shipping'){
              tmp = x[i].settings;
              break;
            }
          }

          if(tmp){
            switch(tmp.requires.value){
              case 'min_amount': // minimun order amount
                if(tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value))
                  this.allowFreeShipping = true;
                break; 
              case 'coupon': // coupon
                if(this.coupon.description) // coupon valid
                  this.allowFreeShipping = true;
                break;
              case 'either': // minimun order amount or coupon
                if(tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value) || this.coupon.description)
                  this.allowFreeShipping = true;
                break;
              case 'both': // minimun order amount and coupon 
                if(tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value) && this.coupon.description)
                  this.allowFreeShipping = true;
                break;
              default:
                this.allowFreeShipping = true;
            }
          }

        }, e=> {
          console.log(e);
        });
      }else{
        this.shipping_method = [];
        this.translate.get(['NO_SHIPPING']).subscribe( x=> {
          this.toast.showWithClose(x.NO_SHIPPING);
        });
      }
    }else{
      this.translate.get(['SELECT_SHIPPING']).subscribe( x=> {
        this.toast.showWithClose(x.SELECT_SHIPPING);
      });
    }
  }

  viewCart(){
    this.modal.create('MiniCartPage', {isCheckout: true}, { cssClass: 'inset-modal' }).present();
  }

  setShipping(param){
    this.shipping_lines = [];
    let e = JSON.parse(param);
    this.shipping_lines.push({
      method_id: e.method_id,
      method_title: e.title,
      total: (e.settings.cost ? e.settings.cost.value : 0)
    });

    this.updateTotal();
    //this.total = this.cart.total + parseInt(this.shipping_lines[0].total);
  }

  setPayment(param){
    this.years = [];
    let e = JSON.parse(param);
    this.order.payment_method = e.id;
    this.order.payment_method_title = e.method_title;
    this.order.payment_method_description = e.description;
    if(e.id != 'cod') this.order.set_paid = true;
    for(let i in this.payments)
      this.payments[i].open = false;
    

    for(let i in this.payments){
      if(this.payments[i].id == e.id){
        this.payments[i].open = true || !this.payments[i].open;
        break;
      }
    }

    if(e.id == 'stripe'){
      if(e.settings.testmode.value == 'yes'){
        this.stripe.publishable_key = e.settings.test_publishable_key.value;
        this.stripe.secret_key = e.settings.test_secret_key.value;
      }else{
        this.stripe.publishable_key = e.settings.publishable_key.value;
        this.stripe.secret_key = e.settings.secret_key.value;
      }

      for(let i=0; i<10; i++)
        this.years.push(new Date().getFullYear() + i);
    }

    if(e.id == 'razorpay'){
      if(e.settings.key_id.value)
        this.razor.key = e.settings.key_id.value;
      //if(e.settings.key_secret.value)
      //  this.razor.key = e.settings.key_secret.value;
    }
  }

  listenOrder(){
    this.events.subscribe('order:go', (res) => {
      this.setOrder();
    });
  }

  selectAddress(action){
    let params = {
      action: action
    }
    this.modal.create('SavedAddressPage', {params: params}).present();
  }

  addAddress(action){
    let params = {
      action: action
    }
    this.modal.create('AddAddressPage', {params: params}).present();
  }

  next(){
    this.checkout = 'payment';
  }

  isCouponValid(x){
    // console.log(this.coupon.input.toLowerCase());
    // console.log(x.toLowerCase());
    return this.coupon.input.toLowerCase() == x.toLowerCase() ? true : false;
  }

  isCouponExpired(x){
    return new Date().getTime() > new Date(x).getTime() ? true : false;
  }

  isMinimumValid(x){
    return this.cart.total < x ? true : false;
  }

  isMaximumValid(x){
    return this.cart.total > x ? true : false;
  }

  submitCoupon(){
    this.allowFreeShipping = false;
    this.order.coupon_lines = [];
    this.coupon.err = '';
    this.coupon.description = '';

    if(this.coupons){
      let errors = [
        'Coupon is invalid', 'Coupon is expired', 'Subtotal must be > ', 'Subtotal must be < ', 'Coupon usage is limited'
      ]
      
      let valid = false;
      let x: any;

      for(let i in this.coupons){
        x = this.coupons[i];
        if(this.isCouponValid(x.code)){
          valid = true;

          if(x.free_shipping)
            this.allowFreeShipping = true;

          break; 
        }
      }

      if(valid){
        if(x.date_expires && this.isCouponExpired(x.date_expires))
          this.coupon.err = errors[1];
        else if(x.minimum_amount > 0 && this.isMinimumValid(x.minimum_amount))
          this.coupon.err = errors[2] + x.minimum_amount;
        else if(x.maximum_amount > 0 && this.isMaximumValid(x.maximum_amount))
          this.coupon.err = errors[3] + x.maximum_amount;
        else if(x.usage_limit && x.usage_count >= x.usage_limit)
          this.coupon.err = errors[4];
        else{
          this.coupon.description = x.description || 'Congrat, your coupon is valid';
          this.order.coupon_lines.push({
            code: x.code,
            discount: this.getTotalDiscount(x)
          });
        }
      }else
        this.coupon.err = errors[0];
      
      this.updateTotal();
    }

    if(this.coupon.err){
      this.translate.get(['OK', 'COUPON_CODE', 'COUPON_INVALID']).subscribe( x=> {
        this.alert.create({
          title: x.COUPON_CODE,
          message: this.coupon.err,
          buttons: [{
              text: x.OK
            }]
        }).present();
      });
    }

  }

  getTotalDiscount(x:any){
    let total = 0;
    if(x.discount_type == 'percent')
      total = x.amount * this.cart.total / 100;
    else if(x.discount_type == 'fixed_cart')
      total = x.amount;
    else 
      total = x.amount * this.cart.totalQtyDetail;  

    return total;
  }

  updateTotal(){
    let shipping = this.shipping_lines[0] ? parseInt(this.shipping_lines[0].total) : 0;
    let discount = this.coupon.description ? parseInt(this.order.coupon_lines[0].discount) : 0;
    this.total = this.cart.total + shipping - discount;
  }
  
  confirm(){
    this.loader.present();
    this.order.billing = this.billing;
    this.order.shipping = this.shipping;
    this.order.line_items = this.cart.lineItems;
    this.order.currency = this.setting.all.settings.currency;

    if(this.coupon.description){ // update line item total if coupon valid
      let tmp: any;
      tmp = this.order.coupon_lines[0].discount / this.cart.totalQtyDetail;
      for(let i in this.order.line_items)
        this.order.line_items[i].total = this.order.line_items[i].subtotal - (tmp.toFixed(2) * this.order.line_items[i].quantity); 
    }

    this.order.shipping_lines = this.shipping_lines;
    if(this.user.all) this.order.customer_id = this.user.id;

    // console.log(this.order);
    // this.loader.dismiss();
    // this.goTo('ThanksPage', this.order);
    // CHECKOUT WITH PAYPAL

    if(this.order.payment_method == 'paypal'){
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else{
        this.order.total = this.total;
        this._order.checkoutPaypal(this.order).then((res)=>{
          if(res){
            this.createOrder(this.order);
          }else{
            this.loader.dismiss();
            this.toast.show(res);
          }
        }, err=>{
          this.loader.dismiss();
          this.toast.show(err);
        });  
      }

    // CHECKOUT WITH STRIPE
    }else if(this.order.payment_method == 'stripe'){
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else{
        this.order.total = this.total;
        this._order.getStripeToken(this.stripe).then((token)=>{
          if(token.id){
            token.secret_key = this.stripe.secret_key;
            this._order.checkoutStripe(this.order, token)
              .subscribe( (res) => {
                  this.createOrder(this.order);
              }, err => {
                this.loader.dismiss();
                this.toast.showWithClose(err.json().error.message);        
              });
          }else{
            this.loader.dismiss();
            this.toast.showWithClose(token);
          }
        }, err => {
          this.loader.dismiss();
          this.toast.showWithClose(err);
        })
      }

    // CHECKOUT WITH RAZOR PAY
    }else if(this.order.payment_method == 'razorpay'){
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else{
        this.order.total = this.total;
        this._order.checkoutRazorpay(this.order, this.razor)
        .then( (res) => {
            this.createOrder(this.order);
        }, err => {
          this.loader.dismiss();
          this.toast.showWithClose(err.description);        
        });
      }

    // CHECKOUT WITH OTHERs
    }else if(this.order.payment_method == 'bacs' || this.order.payment_method == 'cod' || this.order.payment_method == 'cheque'){
      this.createOrder(this.order);

    // NOT AVAILABLE
    }else{
      this.loader.dismiss();
      this.translate.get(['OK', 'NOT_AVAILABLE', 'PAYMENT_NOT_AVAILABLE']).subscribe( x=> {
        this.alert.create({
          title: x.NOT_AVAILABLE,
          message: x.PAYMENT_NOT_AVAILABLE,
          buttons: [{
              text: x.OK
            }]
        }).present();
        this.loader.dismiss();
        return false;
      });
    }
  }

  

  shouldDeviceOnly(){
    this.translate.get(['OK', 'ONLY_DEVICE', 'ONLY_DEVICE_DESC']).subscribe( x=> {
      this.alert.create({
        title: x.ONLY_DEVICE,
        message: x.ONLY_DEVICE_DESC,
        buttons: [{
            text: x.OK
          }]
      }).present();
      this.loader.dismiss();
      return false;
    });
  }

  createOrder(order: any){
    this.woo.createOrder(order).then( x=>{
      if(x){
        this._order.reset().then(() => {});
        this.cart.reset().then(() => {});
        this.goTo('ThanksPage', x);
      }else{
        this.toast.showWithClose('CORS issues, please use https.');
      }
      this.loader.dismiss();
    }, err=> {
      this.loader.dismiss();
      this.toast.show(err);
    })
  }

  goTo(page, params){
    this.app.getRootNav().setRoot(page, {params: params});
  }

}
