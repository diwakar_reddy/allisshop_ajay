import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { App } from '../../app/app.global';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Stripe } from '@ionic-native/stripe';
import { Http, Response, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; 

@Injectable()
export class OrderProvider {
  private ORDER_KEY: string = 'order';

  order: any = {};
  woo: any;

  _readyPromise: Promise<any>;

  constructor(private storage: Storage, private http: Http, private stripe: Stripe, private events: Events, private payPal: PayPal) {
    this.load();
  }

  load() {
    return this.storage.get(this.ORDER_KEY).then((val) => {
      if (val) {
        this.order = val;
        return this.order;
      } else {
        this.save();
      }
    });
  }

  setBilling(billing: any){
    this.order.billing = billing;
    this.events.publish('order:go');
    return this.save();
  }

  setShipping(shipping: any){
    this.order.shipping = shipping;
    this.events.publish('order:go');
    return this.save();
  }

  get billing(){
    return this.order.billing;
  }

  get shipping(){
    return this.order.shipping;
  }

  getStripeToken(opt: any){
    this.stripe.setPublishableKey(opt.publishable_key);
    let card = {
      number: opt.no,
      expMonth: opt.month,
      expYear: opt.year,
      cvc: opt.cvc
    }
    return this.stripe.createCardToken(card)
    .then(token => {
      return token;
    }, (err) => {
      return err;
    });
  }

  post(url: string, key: string, options: any){
    let data = new URLSearchParams(options);
    for(let i in options)
      data.set(i, options[i]);

    let header = new Headers();
    header.set('access-control-allow-credentials', 'true');
    header.set('access-control-allow-methods', 'GET, POST, HEAD, OPTIONS, DELETE');
    header.set('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
    header.set('Authorization', 'Bearer ' + key);

    let option = new RequestOptions({headers: header});

    let seq = this.http.post(url, data.toString(), option);
    seq.map((res:Response) => {
      res.json();
    });
    
    return seq;
  }

  checkoutStripe(order: any, token: any){
    let url = 'https://api.stripe.com/v1/charges';
    let options = {
      amount: order.total * 100,
      currency: order.currency,
      description: 'Checkout from ' + order.billing.first_name + ' - ' + order.total,
      receipt_email: order.billing.email,
      'shipping[phone]': order.shipping_lines[0].method_title,
      'shipping[carrier]': order.carrier,
      'shipping[name]': order.shipping.first_name,
      'shipping[address][line1]': order.shipping.address_1,
      'shipping[address][postal_code]': order.shipping.postcode,
      'shipping[address][city]': order.shipping.city,
      'shipping[address][state]': order.shipping.state,
      'shipping[address][country]': order.shipping.country,
      'metadata[Customer Email]': order.billing.email,
      'metadata[Customer Name]': order.billing.first_name,
      'metadata[Customer Device]': order.device,
      'source': token.id
    }
    let seq = this.post(url, token.secret_key, options);
    return seq;
  }

  checkoutRazorpay(order: any, token: any){
    return new Promise((resolve, reject) => {
      var cancelCallback = (error) => {
        reject(error);
      } 

      var successCallback = (success) => {
        resolve(success);
      }

      var options = {
        description: 'Checkout with Razorpay',
        image: App.logo,
        currency: 'INR',
        key: token.key,
        amount: order.total * 100,
        name: App.title,
        prefill: {
          email: order.billing.email,
          contact: order.billing.phone,
          name: order.billing.first_name + ' ' + order.billing.last_name
        },
        theme: {
          color: App.RazorColor,
        },
        modal: {
          ondismiss: function() {
            // alert('dismissed');
            console.log('dismissed');
          }
        }
      };

      RazorpayCheckout.on('payment.success', successCallback);
      RazorpayCheckout.on('payment.cancel', cancelCallback);
      RazorpayCheckout.open(options);  
    });
  }

  checkoutMollie(order: any){
    let url = 'https://api.mollie.nl/v1/payments';
    let options = {
      amount: order.total,
      method: order.payment_method.substring(18),
      redirectUrl: 'https://localhost/order',
      webhookUrl: 'https://example.org/payments/webhook',
      description: 'Checkout from ' + order.billing.first_name + ' - ' + order.total,
      'metadata[device]': order.device,
      'metadata[shipment][title]': order.shipping_lines[0].method_title,
      'metadata[shipment][cost]': order.shipping_lines[0].total,
      'metadata[billing][name]': order.billing.first_name,
      'metadata[billing][address_1]': order.billing.address_1,
      'metadata[billing][city]': order.billing.city,
      'metadata[billing][phone]': order.billing.phone,
      'metadata[billing][state]': order.billing.state,
      'metadata[billing][postcode]': order.billing.postcode,
      'metadata[billing][country]': order.billing.country,
      'metadata[shipping][name]': order.shipping.first_name,
      'metadata[shipping][address_1]': order.shipping.address_1,
      'metadata[shipping][city]': order.shipping.city,
      'metadata[shipping][phone]': order.shipping.phone,
      'metadata[shipping][state]': order.shipping.state,
      'metadata[shipping][postcode]': order.shipping.postcode,
      'metadata[shipping][country]': order.shipping.country
    }
    let seq = this.post(url, App.mollieKey, options)
    return seq;
  }

  checkoutPaypal(order: any){
    let options = {
      total: order.total,
      currency: order.currency,
      desc: order.payment_method_description,
      detail: {
        subtotal: order.total,
        shipping: order.shipping_lines[0].total
      }
    }

    return this.payPal.init({
      PayPalEnvironmentProduction: App.paypalLiveClientID,
      PayPalEnvironmentSandbox: App.paypalSandboxClientID
    }).then(() => {
      return this.payPal.prepareToRender(App.paypalState, new PayPalConfiguration({
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        // let paymentDetail = new PayPalPaymentDetails(
        //   options.detail.subtotal.toFixed(2), 
        //   options.detail.shipping.toFixed(2), '0.00'
        // );
        let payment = new PayPalPayment(options.total.toFixed(2), options.currency, options.desc, 'sale', null);
        return this.payPal.renderSinglePaymentUI(payment).then((x) => {
          return x;
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }

  reset(){
    this.order = {};
    return this.storage.remove(this.ORDER_KEY).then(() => {});
  }

  save(){
    return this.storage.set(this.ORDER_KEY, this.order);
  }
}
