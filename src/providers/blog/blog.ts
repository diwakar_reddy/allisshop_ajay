import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { App } from '../../app/app.global';

@Injectable()
export class BlogProvider {

  constructor(private http: Http) {
    App.url = App.url;
  }

  get(page: number = 1){
    let p = page ? 'page='+page : 'page=1';
    let seq = this.http.get(App.url+'/get_posts/?'+p);
    seq.map((res:Response) => {});
    return seq;
  }

}
